package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import model.Building;

@SuppressWarnings("serial")
public class UtilityPanel extends JPanel implements Runnable{

	private static int WIDTH;
	private static int HEIGHT;
	private boolean running;
	private Thread thread;
	JButton buildPoop = new JButton("POOPING PLACE");
	private Building buildings;
	
	public UtilityPanel() {
		super();
		buildings = new Building();
		this.setPreferredSize(new Dimension((GamePanel.getScreenWidth() / 5), GamePanel.getScreenHeight()));
		this.setBackground(Color.ORANGE);
		this.add(buildPoop);
		this.setVisible(true);
		
		buildPoop.addActionListener(new ActionListener()
		{
		      public void actionPerformed(ActionEvent e)
		      {
		    	  buildings.buildBuilding(0);
		      }
		    });
	}
	
	public void addNotify() {
		super.addNotify();
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void run() {}
	
	private void init() {}
	
	private void update() {}
	
	private void handleMouseMoved(int x, int y) {}
	
	private void handleMousePressed(int x, int y) {}
	
	private void utilityDraw() {}
	
}
