package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

import data.AStarPathFinder;
import data.GameClock;
import data.Path;
import data.Tilemap;
import data.UnitMover;
import model.Creature;

@SuppressWarnings("serial")
public class GamePanel extends JPanel implements Runnable{
	private static int WIDTH = 600;
	private static int HEIGHT = 600;
	
	private int selectedCharacterX = -1;
	private int selectedCharacterY = -1;
	private int lastFindX = -1;
	private int lastFindY = -1;
	private int mouseX;
	private int mouseY;
	
	private boolean running;
	
	private BufferedImage image;
	private Graphics g;
	private Thread thread;
	public static Tilemap tileMap;
	private PathFinder pathFinder;
	private Path path;
	public static GameClock gameClock;
	
	public GamePanel() {
		super();
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		this.setBackground(Color.BLACK);
		this.setVisible(true);
	}
	
	public void addNotify() {
		super.addNotify();
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}
	
	public void run() {
		
		init();
		
		// Creating a human for test purposes
		// tag = 1 (human), placed on xy = 3,3, height&width 15*15 and speed 1
		Creature creature = new Creature(1, 3, 3, 1);
		creature.setCreatureToField();
		
		while(running) {
			gameClock.FramesPerSecond();
			gameClock.Update();
			gameUpdate();
			gameDraw();
			gameRender();
		}
	}
	
	private void init() {
		running = true;
		image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		g = (Graphics2D) image.getGraphics();
		tileMap = new Tilemap("testMap.txt", 15);
		pathFinder = new AStarPathFinder(tileMap, 1000, true);
		gameClock = new GameClock();
		
		addMouseListener (new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				handleMousePressed(e.getX(), e.getY());
			}
		});
		
		addMouseMotionListener(new MouseMotionListener() {

			@Override
			public void mouseDragged(MouseEvent e) {
			}

			@Override
			public void mouseMoved(MouseEvent e) {
				handleMouseMoved(e.getX(), e.getY());
			}
			
		});
	}
	
	private void handleMouseMoved(int x, int y) {
		// These have to change with tile width && tile height && dimensions for the mouse x, y to match
		mouseX = x;
		mouseY = y;
		x /= 15;
		y /= 15;
		if (x < 0 || y < 0 || x >= tileMap.getMapWidth() || y >= tileMap.getMapHeight()) {
			return;
		}
		if (selectedCharacterX != -1) {
			if ((lastFindX != x) || (lastFindY != y)) {
				lastFindX = x;
				lastFindY = y;
				path = pathFinder.findPath(new UnitMover(tileMap.getCreatures(selectedCharacterX, selectedCharacterY)), selectedCharacterX, selectedCharacterY, x, y);
			}
		}
	}
	
	private void handleMousePressed(int x, int y) {
		// These have to change with tile width && tile height && dimensions for the mouse x, y to match
		x /= 15;
		y /= 15;
		if (x < 0 || y < 0 || x >= tileMap.getMapWidth() || y >= tileMap.getMapHeight()) {
			return;
		}
		if (tileMap.getCreatures(x, y) != 0) {
			selectedCharacterX = x;
			selectedCharacterY = y;
			lastFindX = - 1;
		} else {
			if (selectedCharacterX != -1) {
				tileMap.clearVisited();
				path = pathFinder.findPath(new UnitMover(tileMap.getCreatures(selectedCharacterX, selectedCharacterY)), selectedCharacterX, selectedCharacterY, x, y);
				moveCreature(selectedCharacterX, selectedCharacterY, x, y, path);
			}
		}
	}
	
	private void gameDraw() {
		Graphics g2 = this.getGraphics();
		g.drawImage(image, 0, 0, null);
		g2.drawImage(image, 0, 0, null);
		g2.dispose();
		tileMap.draw(g);
	}

	private void gameRender() {
		
		// Draw the creatures in our creatures[][] -- in this case human (!0 == red)
		for (int x = 0; x < tileMap.getMapWidth(); x++) {
			for (int y = 0; y < tileMap.getMapHeight(); y++) {
				if (tileMap.getCreatures(x, y) != 0) {
					g.setColor(Color.RED);
					g.fillRect(tileMap.getX() + x * tileMap.getTileSize(), tileMap.getY() + y * tileMap.getTileSize(), tileMap.getTileSize(), tileMap.getTileSize());
				} else {
					// Draw the planned route
					if (path != null) {
						if (path.contains(x, y)) {
							g.setColor(Color.YELLOW);
							g.fillRect((x*15)+4, (y*15)+4,7,7);
						}
					}	
				}
			}
		}
		// Draw a border around the selected creature
		if (selectedCharacterX != -1) {
			g.setColor(Color.black);
			g.drawRect(selectedCharacterX*15, selectedCharacterY*15, 15, 15);
			g.drawRect(selectedCharacterX*15-2, selectedCharacterY*15-2, 19, 19);
			g.setColor(Color.white);
			g.drawRect(selectedCharacterX*15-1, selectedCharacterY*15-1, 17, 17);
			g.drawImage(image, 0, 0, null);
		}
		// Show frames per second
		g.setColor(Color.RED);
		g.setFont(new Font("Dialog", Font.BOLD, 18));
		g.drawString("FPS: "+gameClock.GetFramesPerSecond(), 15, 15);
	}

	private void gameUpdate() {
	}
	
	public void moveCreature(int selectedCharacterX, int selectedCharacterY, int x, int y, Path path) {
		if (path != null) {
			int unit = tileMap.getCreatures(selectedCharacterX, selectedCharacterY);
			for (int i = 1; i < path.getLength(); i++) {
				long timeNow = gameClock.getTimeInMillis();
				long timeToContinue = timeNow;
				while (timeToContinue < timeNow + 500) {
					timeToContinue = gameClock.getTimeInMillis();
				}
				tileMap.setCreatures(path.getX(i-1), path.getY(i-1), 0);
				tileMap.setCreatures(path.getX(i), path.getY(i), unit);
			}
			selectedCharacterX = x;
			selectedCharacterY = y;
			lastFindX = - 1;
			path = null;
		}
	}
	
	public void placeBuilding(int value) {
		g.setColor(Color.ORANGE);
		g.drawRect(mouseX, mouseY, 15*value, 15*value);
	}
	
	public static int getScreenHeight() {
		return HEIGHT;
	}
	
	public static int getScreenWidth() {
		return WIDTH;
	}
}
