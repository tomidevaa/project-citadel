package view;

public interface TilemapInterface {
	
	public void pathFinderVisited(int x, int y);
	public boolean isBlocked(Mover mover, int targetX, int targetY);
	public float getCost(Mover mover, int startX, int startY, int targetX, int targetY);
	public int getMapWidth();
	public int getMapHeight();
}
