package view;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GameBoard extends JFrame{
	private GamePanel myGamePanel;
	private UtilityPanel myUtilityPanel;
	
	public GameBoard() {
		super("Project Citadel");
		myGamePanel = new GamePanel();
		myUtilityPanel = new UtilityPanel();
		start();
	}
	
	public void start() {
		setVisible(true);
		buildGUI();
	}
	
	public void buildGUI() {
		JPanel masterPanel = new JPanel();
		masterPanel.setLayout(new BorderLayout());
		setLocationRelativeTo(null);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		masterPanel.add(myGamePanel, BorderLayout.CENTER);
		masterPanel.add(myUtilityPanel, BorderLayout.EAST);
		add(masterPanel);
		pack();
	}

}
