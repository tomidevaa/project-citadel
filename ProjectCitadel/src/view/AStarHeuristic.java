package view;

import data.Tilemap;

/**
 * Get the additional heuristic cost of the given tile. This controls the
 * order in which tiles are searched while attempting to find a path to the 
 * target location. The lower the cost the more likely the tile will
 * be searched.
 * 
 * @param map The map on which the path is being found
 * @param mover The entity that is moving along the path
 * @param startX The x coordinate of the start location
 * @param startY The y coordinate of the start location
 * @param targetX The x coordinate of the target location
 * @param targetY The y coordinate of the target location
 * @return The cost associated with the given tile
 */
public interface AStarHeuristic {
	
	public float getCost(TilemapInterface tilemapInterface, Mover mover, int startX, int startY, int targetX, int targetY);
}
