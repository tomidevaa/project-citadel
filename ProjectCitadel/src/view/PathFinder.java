package view;

import data.Path;

/**
 * Find a path from the starting location provided (sx,sy) to the target
 * location (tx,ty) avoiding blockages and attempting to honour costs 
 * provided by the tile map.
 * 
 * @param mover The entity that will be moving along the path. This provides
 * a place to pass context information about the game entity doing the moving, e.g.
 * can it fly? can it swim etc.
 * 
 * @param startX The x coordinate of the start location
 * @param startY The y coordinate of the start location
 * @param targetX The x coordinate of the target location
 * @param targetY The y coordinate of the target location
 * @return The path found from start to end, or null if no path can be found.
 */

public interface PathFinder {
	
	public Path findPath(Mover mover, int startX, int startY, int targetX, int targetY);
}
