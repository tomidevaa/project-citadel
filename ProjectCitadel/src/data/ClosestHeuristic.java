package data;

import view.AStarHeuristic;
import view.Mover;
import view.TilemapInterface;

public class ClosestHeuristic implements AStarHeuristic {
	
	public float getCost(TilemapInterface tilemapInterface, Mover mover, int startX, int startY, int targetX, int targetY) {
		float distanceX = targetX - startX;
		float distanceY = targetY - startY;
		float result = (float) (Math.sqrt((distanceX*distanceX)+(distanceY*distanceY)));
		
		return result;
	}
}
