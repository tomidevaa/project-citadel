package data;

public class GameClock {
	
	private static boolean paused = false;
	public static long lastFrame;
	public static long totalTime;
	// Delta holder d
	public static float d = 0;
	public float multiplier = 1;
	private long deltaForFrames = 0;
	private int frames = 0;
	private int framesPerSecond = 0;
	
	// Get time in milliseconds
	public long getTimeInMillis() {
		return (System.nanoTime() / 1000000L);
	}
	
	// Get the time difference between last update cycle and the current one.
	// The method
	// 1. Gets current time
	// 2. Stores the difference by deducting the time stored during last cycle
	// 3. Stores the current time to be used in step 2 next cycle
	public long getDeltaTime() {
		long currentTimeInMillis = getTimeInMillis();
		long delta = (currentTimeInMillis - lastFrame);
		deltaForFrames += delta;
		lastFrame = getTimeInMillis();
		return delta;
	}
	
	public float Delta() {
		if (paused) {
			return 0;
		} else {
			return d * multiplier;
		}
	}
	
	public float TotalTime() {
		return totalTime;
	}
	
	public float Multiplier() {
		return multiplier;
	}
	
	public void ChangeMultiplier(int change) {
		if (multiplier + change < -1 && multiplier + change > 7) {
			
		} else {
			multiplier += change;
		}
	}
	
	public void FramesPerSecond() {
	}
	
	public void Pause() {
		if (paused) {
			paused = false;
		} else {
			paused = true;
		}
	}
	
	public int GetFramesPerSecond() {
		frames++;
		if (deltaForFrames >= 1000) {
			deltaForFrames = 0;
			framesPerSecond = frames;
			System.out.println("FPS: " + framesPerSecond);
			frames = 0;
		}
		
		return framesPerSecond;
	}
	
	public void Update() {
		GetFramesPerSecond();
		d = getDeltaTime();
		totalTime += d;
	}

}
