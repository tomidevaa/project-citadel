package data;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.BufferedReader;
import java.io.FileReader;

import model.Creature;
import view.Mover;
import view.TilemapInterface;

public class Tilemap implements TilemapInterface {
	private int x;
	private int y;
	private int[][] map;
	public int[][] creatures;
	public boolean[][] visited;
	private int tileSize;
	private int mapWidth;
	private int mapHeight;
	
	public Tilemap(String file, int tileSize) {
		this.tileSize = tileSize;
		try {
			BufferedReader bReader = new BufferedReader(new FileReader(file));
			mapWidth = Integer.parseInt(bReader.readLine());
			mapHeight = Integer.parseInt(bReader.readLine());
			creatures = new int[mapHeight][mapWidth];
			visited = new boolean[mapHeight][mapWidth];
			map = new int[mapHeight][mapWidth];
			String delimiters = " ";
			for (int y = 0; y < mapHeight; y++) {
				String line = bReader.readLine();
				String[] tokens = line.split(delimiters);
				for (int x = 0; x < mapWidth; x++) {
					map[y][x] = Integer.parseInt(tokens[x]);
				}
			}
			bReader.close();
		} catch (Exception e) {
			System.out.println("Error from Board.class: " + e);
		}
	}
	
	public void clearVisited() {
		for (int y = 0; y < getMapWidth(); y++) {
			for (int x = 0; x < getMapHeight(); x++) {
				visited[y][x] = false;
			}
		}
	}
	
	public boolean visited(int x, int y) {
		return visited[x][y];
	} 
	
	public void draw(Graphics g) {
		for (int i = 0; i < mapHeight; i++) {
			for (int j = 0; j < mapWidth; j++) {
				int rc = map[j][i];
				if (rc == 1) {
					// 1 == water, can't swim
					g.setColor(Color.BLUE);
				} else if (rc == 2) {
					// 2 == grass, movable
					g.setColor(Color.GREEN);
				} else if (rc == 3) {
					// 3 == high mountains, can't climb
					g.setColor(Color.DARK_GRAY);
				} 
				g.fillRect(getX() + j * getTileSize(), getY() + i * getTileSize(), getTileSize(), getTileSize());
			}
		}
	}
	
	public void update() {
		
	}
	
	public int getTileSize() {
		return tileSize;
	}
	
	public int getTileX(int x) {
		return x / tileSize;
	}
	
	public int getTileY(int y) {
		return y / tileSize;
	}
	
	public int getTile(int x, int y) {
		return map[x][y];
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getMapWidth() {
		return mapWidth;
	}

	public int getMapHeight() {
		return mapHeight;
	}
	
	public int getCreatures(int x, int y) {
		return creatures[x][y];
	}
	
	public void setCreatures(int x, int y, int creature) {
		creatures[x][y] = creature;
	}
	
	public boolean isBlocked(Mover mover, int x, int y) {
		// If tile is blocked by another creature, return blocked
		if (getCreatures(x, y) != 0) {
			return true;
		}
		int creature = ((UnitMover) mover).getMoverType();
		// If creature is human, can move on grass
		if (creature == 1) {
			return getTile(x, y) != 2;
		}
		// block everything else, aka unknown creatures
		return true;
	}
	
	public float getCost(Mover mover, int startX, int startY, int targetX, int targetY) {
		return 1;
	}
	
	public void pathFinderVisited(int x, int y) {
		visited[x][y] = true;
	}
}
