package data;

import view.GameBoard;

public class CitadelMain {
	private static Tilemap myBoard;
	private static GameBoard myGameBoard;
	
	public static void main(String[] args) {
		// myGameBoard is the front end
		myGameBoard = new GameBoard();
		myGameBoard.start();
	}
}
