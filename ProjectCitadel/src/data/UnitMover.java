package data;

import view.Mover;

public class UnitMover implements Mover {
	
	// 0 == human
	private int moverType;
	
	public UnitMover(int moverType) {
		this.moverType = moverType;
	}
	
	public int getMoverType() {
		return moverType;
	}
}
