package data;

import java.util.ArrayList;
import java.util.Collections;

import view.AStarHeuristic;
import view.Mover;
import view.PathFinder;
import view.TilemapInterface;

public class AStarPathFinder implements PathFinder {
	private ArrayList discardedList = new ArrayList();
	private SortedList openNodesList = new SortedList();
	private TilemapInterface tilemapInterface;
	private int maxSearchDist;
	private Node[][] nodes;
	private boolean allowDiagMove;
	private AStarHeuristic heuristic;
	
	// Path finder - closest to
	public AStarPathFinder(TilemapInterface tilemapInterface, int maxSearchDist, boolean allowDiagMove) {
		this(tilemapInterface, maxSearchDist, allowDiagMove, new ClosestHeuristic());
	}
	
	// Path finder  .. alternative
	public AStarPathFinder(TilemapInterface tilemapInterface, int maxSearchDist, boolean allowDiagMove, AStarHeuristic heuristic) {
		this.tilemapInterface = tilemapInterface;
		this.maxSearchDist = maxSearchDist;
		this.allowDiagMove = allowDiagMove;
		this.heuristic = heuristic;
		
		nodes = new Node[tilemapInterface.getMapHeight()][tilemapInterface.getMapWidth()];
		for (int x = 0; x < tilemapInterface.getMapWidth(); x++) {
			for (int y = 0; y < tilemapInterface.getMapHeight(); y++) {
				nodes[x][y] = new Node(x, y);
			}
		}
	}
	
	public Path findPath(Mover mover, int startX, int startY, int targetX, int targetY) {
		if (tilemapInterface.isBlocked(mover, targetX, targetY)) {
			return null;
		}
		// init A*: clear the discardedList and refill the openNodesList
		nodes[startX][startY].cost = 0;
		nodes[startX][startY].depth = 0;
		discardedList.clear();
		openNodesList.clear();
		openNodesList.addAndSort(nodes[startX][startY]);
		nodes[targetX][targetY].parent = null;
		int maxDepth = 0;
		// while not over the max depth
		while (maxDepth < maxSearchDist && openNodesList.listSize() != 0) {
			Node currentNode = getFirstFromOpenNodesList();
			// if target is same as start...
			if (currentNode == nodes[targetX][targetY]) {
				break;
			}
			// Remove from open nodes and add to discarded
			removeFromOpenNodesList(currentNode);
			addToDiscardedList(currentNode);
			// Look through neighbour nodes
			for (int x = -1; x < 2; x++) {
				for (int y = -1; y < 2; y++) {
					// Skip current tile
					if (x == 0 && y == 0 ) {
						continue;
					}
					// Let's give an option for non-diagonal movement allowed:
					// If not allowed, only one X and one Y possible.
					if (!allowDiagMove) {
						if (x != 0 && y != 0) {
							continue;
						}
					}
					// Location of neighbour
					int possibleX = x + currentNode.x;
					int possibleY = y + currentNode.y;
					// Check validity of movement
					if (isValidLocation(mover, startX, startY, possibleX, possibleY)) {
						float costForStep = currentNode.cost + getMovementCost(mover, currentNode.x, currentNode.y, possibleX, possibleY);
						Node next = nodes[possibleX][possibleY];
						tilemapInterface.pathFinderVisited(possibleX, possibleY);
						// Re-evaluate if a cheaper neighbour is found
						if (costForStep < next.cost) {
							if (inOpenNodesList(next)) {
								removeFromOpenNodesList(next);
							}
							if (inDiscardedList(next)) {
								removeFromDiscarded(next);
							}
						}
						// If node is in neither list then start the process over from
						// the said node and add it as a possible step
						if (!inOpenNodesList(next) && !inDiscardedList(next)) {
							next.cost = costForStep;
							next.heuristic = getHeuristicCost(mover, possibleX, possibleY, targetX, targetY);
							maxDepth = Math.max(maxDepth, next.setParent(currentNode));
							addToOpenNodesList(next);
						}
					}
				}
			}
		}
		// If we got here then it's a null pathing
		if (nodes[targetX][targetY].parent == null) {
			return null;
		}
		// Use parent references to record a path from target to start
		Path path = new Path();
		Node target = nodes[targetX][targetY];
		while (target != nodes[startX][startY]) {
			path.prependStep(target.x, target.y);
			target = target.parent;
		}
		path.prependStep(startX, startY);
		
		return path;
	}
	
	protected Node getFirstFromOpenNodesList() {
		return (Node) openNodesList.firstNode();
	}
	
	protected void addToOpenNodesList(Node node) {
		openNodesList.addAndSort(node);
	}
	
	// Checks if given node is available
	protected boolean inOpenNodesList(Node node) {
		return openNodesList.containsElement(node);
	}
	
	protected void removeFromOpenNodesList(Node node) {
		openNodesList.remove(node);
	}
	
	protected void addToDiscardedList(Node node) {
		discardedList.add(node);
	}
	
	// Checks if given node has been discarded
	protected boolean inDiscardedList(Node node) {
		return discardedList.contains(node);
	}
	
	protected void removeFromDiscarded(Node node) {
		discardedList.remove(node);
	}
	// Returns true if target position is a valid move.
	// Assigns false value if mover within map limits. Further examines the falseness if target pos is not the same as start pos.
	// If the target pos is legit, assigns the value isBlocked = false and returns !value.
	protected boolean isValidLocation(Mover mover, int startX, int startY, int targetX, int targetY) {
		boolean invalid = targetX < 0 || targetY < 0 || targetX >= tilemapInterface.getMapWidth() || targetY >= tilemapInterface.getMapHeight();
		if (!invalid && (startX != targetX || startY != targetY)) {
			invalid = tilemapInterface.isBlocked(mover, targetX, targetY);
		}
		return !invalid;
	}
	// Gets movement cost through a given target location
	public float getMovementCost(Mover mover, int startX, int startY, int targetX, int targetY) {
		return tilemapInterface.getCost(mover, startX, startY, targetX, targetY);
	}
	// Gets the heuristic cost from point a to point b, determines the order of locations
	public float getHeuristicCost(Mover mover, int startX, int startY, int targetX, int targetY) {
		return heuristic.getCost(tilemapInterface, mover, startX, startY, targetX, targetY);
	}
	
	private class SortedList {
		private ArrayList list = new ArrayList();
		
		public Object firstNode() {
			return list.get(0);
		}
		
		public void clear() {
			list.clear();
		}
		
		public void addAndSort(Object o) {
			list.add(o);
			Collections.sort(list);
		}
		
		public void remove(Object o) {
			list.remove(o);
		}
		
		public int listSize() {
			return list.size();
		}
		
		public boolean containsElement(Object o) {
			return list.contains(o);
		}
	}
	
	private class Node implements Comparable {
		private int x;
		private int y;
		private int depth;
		private float cost;
		private float heuristic;
		private Node parent;
		
		public Node(int x, int y) {
			this.x = x;
			this.y = y;
		}
		
		public int setParent(Node parent) {
			depth = parent.depth + 1;
			this.parent = parent;
			
			return depth;
		}
		
		public int compareTo(Object o) {
			Node other = (Node) o;
			float f = heuristic + cost;
			float otherF = other.heuristic + other.cost;
			if (f < otherF) {
				return -1;
			} else if (f > otherF) {
				return 1;
			} else {
				return 0;
			}
		}
	}
}
