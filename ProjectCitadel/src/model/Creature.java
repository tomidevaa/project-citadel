package model;

import data.GameClock;
import data.Tilemap;
import view.GamePanel;

public class Creature {
	
	private int x;
	private int y;
	private float speed;
	private int creatureTag;
	private boolean first = true;
	private Tilemap tilemap;
	private GameClock gameClock;
	
	// Tag for now represents the creature type, 1 == human
	public Creature(int tag, int x, int y, float speed) {
		this.creatureTag = tag;
		this.x = x;
		this.y = y;
		this.speed = speed;
		tilemap = GamePanel.tileMap;
		gameClock = GamePanel.gameClock;
	}
	
	public void Update() {
	}
	
	public void setCreatureToField() {
		if (!(tilemap.creatures == null)) {
			tilemap.creatures[y][x] = creatureTag;
		} else {
			tilemap.creatures = new int[tilemap.getMapHeight()][tilemap.getMapWidth()];
			tilemap.creatures[y][x] = creatureTag;
		}
	}
}
	
