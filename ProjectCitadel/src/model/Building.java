package model;

import java.util.HashMap;

import view.GamePanel;

public class Building {
	
	private HashMap <Integer, Integer> buildings = new HashMap<>();
	private GamePanel gamePanel;
	
	public Building() {
		gamePanel = new GamePanel();
		buildings.put(0, 3);
	}
	
	public void buildBuilding(Integer key) {
		System.out.println("Building number 0 (Pooping place) chosen.");
		gamePanel.placeBuilding(buildings.get(key));
	}

}
